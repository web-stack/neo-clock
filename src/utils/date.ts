export function dateToTimeString(date: Date): string {
  return `${prependZero(date.getHours())}:${prependZero(date.getMinutes())}`;
}

function prependZero(num: number): string {
  return num < 10 ? `0${num}` : num + "";
}

export function addTimezone(date: Date, timezone: number) {
  const diff = timezone * 60 + date.getTimezoneOffset();
  return new Date(date.getTime() + diff * 60 * 1000);
}
