import { useApplicationContext } from "~app/App";

type Classes = {
  light: string;
  dark: string;
};

export function useThemedClasses(classes: Classes) {
  const store = useApplicationContext();
  return store.theme === "dark" ? classes.dark : classes.light;
}
