import { Theme } from "~app/App.store";

interface Settings {
  theme: Theme;
  citiesList: string[];
}

export default Settings;
