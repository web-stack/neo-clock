import "mobx-react-lite/batchingForReactDom";
import React from "react";
import { render } from "react-dom";
import { HashRouter } from "react-router-dom";
import App from "~app/App";
import NavigationServiceProvider from "~service/navigation/NavigationServiceProvider";
import SettingsServiceProvider from "~service/settings/SettingsServiceProvider";
import TimezoneServiceProvider from "~service/timezone/TimezoneServiceProvider";

render(
  <HashRouter>
    <TimezoneServiceProvider>
      <NavigationServiceProvider>
        <SettingsServiceProvider>
          <App />
        </SettingsServiceProvider>
      </NavigationServiceProvider>
    </TimezoneServiceProvider>
  </HashRouter>,
  document.getElementById("root")
);
