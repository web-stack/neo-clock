import clsx from "clsx";
import React from "react";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Switch.scss";

interface IProps {
  isOn: boolean;
  onChange?: (isOn: boolean) => void;
}

function Switch(props: IProps) {
  const rootClasses = useThemedClasses({
    light: styles.rootLight,
    dark: styles.rootDark,
  });

  const dotClasses = useThemedClasses({
    light: styles.dotLight,
    dark: styles.dotDark,
  });

  const handleChange = () => {
    if (props.onChange) {
      props.onChange(!props.isOn);
    }
  };

  return (
    <div className={clsx(styles.root, rootClasses)} onClick={handleChange}>
      <div
        className={clsx(styles.dot, dotClasses, props.isOn && styles.isOn)}
      />
    </div>
  );
}

export default Switch;
