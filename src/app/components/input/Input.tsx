import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React from "react";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Input.scss";

interface IProps {
  value: string;
  placeholder?: string;
  onChange?: (value: string) => void;
}

function Input(props: IProps) {
  const classes = useThemedClasses({
    dark: styles.rootDark,
    light: styles.rootLight,
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (props.onChange) {
      props.onChange(e.target.value);
    }
  };

  return (
    <input
      type="text"
      value={props.value}
      onChange={handleChange}
      className={clsx(styles.root, classes)}
      placeholder={props.placeholder}
    />
  );
}

export default observer(Input);
