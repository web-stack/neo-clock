import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Button.scss";

interface IProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {}

function Button(props: IProps) {
  const [state, setState] = useState({ clicked: false });
  const classes = useThemedClasses({
    light: styles.rootLight,
    dark: styles.rootDark,
  });
  const animationClasses = useThemedClasses({
    light: styles.clickAnimation__light,
    dark: styles.clickAnimation__dark,
  });

  const handleClick = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    setState({ ...state, clicked: true });
    if (props.onClick) {
      props.onClick(e);
    }
  };
  const handleAnimationEnd = () => {
    setState({ ...state, clicked: false });
  };

  return (
    <button
      {...props}
      onClick={handleClick}
      onAnimationEnd={handleAnimationEnd}
      className={clsx(
        styles.root,
        classes,
        props.className,
        state.clicked && animationClasses
      )}
    >
      <div className={clsx(styles.text, state.clicked && styles.textClicked)}>
        {props.children}
      </div>
    </button>
  );
}

export default observer(Button);
