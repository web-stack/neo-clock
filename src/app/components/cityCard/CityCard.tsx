import React from "react";
import Card from "~app/components/card/Card";
import { dateToTimeString } from "~utils/date";
import styles from "./CityCard.scss";

export interface ICityCardProps {
  city: string;
  time: Date;
}

function CityCard(props: ICityCardProps) {
  return (
    <Card className={styles.cardRoot}>
      <div>{props.city}</div>
      <div>{dateToTimeString(props.time)}</div>
    </Card>
  );
}

export default CityCard;
