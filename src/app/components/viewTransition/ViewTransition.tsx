import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React, {
  Context,
  createContext,
  useContext,
  useRef,
  useState,
} from "react";
import { ViewState } from "~service/navigation/Page";
import IHasChildren from "~utils/IHasChildren";
import { useThemedClasses } from "~utils/Theming";
import styles from "./ViewTransition.scss";

type IContext = {
  element: HTMLDivElement | undefined;
  viewHasLeftScreen: boolean;
};

const Context = createContext<IContext | undefined>(undefined);

const stylesMap = {
  leaving: styles.leaving,
  hidden: styles.hidden,
};

interface IProps extends IHasChildren {
  viewState?: ViewState;
}

function ViewTransition({ viewState, children }: IProps) {
  const ref = useRef<HTMLDivElement>();
  const [state, setState] = useState({ left: viewState === "hidden" });

  const handleTransitionEnd = () => {
    if (viewState === "leaving" || viewState === "hidden") {
      setState({ left: true });
    }

    if (typeof viewState === "undefined") {
      setState({ left: false });
    }
  };
  const classes = useThemedClasses({
    light: styles.rootLight,
    dark: styles.rootDark,
  });

  return (
    <div
      ref={(r: HTMLDivElement) => (ref.current = r)}
      onTransitionEnd={handleTransitionEnd}
      className={clsx(styles.root, classes, viewState && stylesMap[viewState])}
    >
      <Context.Provider
        value={{
          element: ref.current,
          viewHasLeftScreen: state.left,
        }}
      >
        {state.left && viewState === "hidden" ? <></> : children}
      </Context.Provider>
    </div>
  );
}

export default ViewTransition;

export function useViewContext() {
  return useContext(Context as Context<IContext>);
}
