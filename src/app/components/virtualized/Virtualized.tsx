import { observer } from "mobx-react-lite";
import React, { ReactNode, useEffect, useRef, useState } from "react";
import { useViewContext } from "~app/components/viewTransition/ViewTransition";

interface IProps<T> {
  source: T[];
  children: (items: T[]) => ReactNode;
  itemsPerPage?: number;
}

function Virtualized<T>({ itemsPerPage = 10, ...props }: IProps<T>) {
  const ref = useRef(1);
  const [state, setState] = useState({ page: 1 });
  const { element, viewHasLeftScreen } = useViewContext();

  useEffect(() => {
    if (viewHasLeftScreen) {
      element?.scrollTo({ top: 0 });
      setState({ page: 1 });
      ref.current = 1;
    }
  }, [viewHasLeftScreen]);

  useEffect(() => {
    if (!element) {
      return;
    }

    function handler(e: Event) {
      if (!element) return;

      if (
        element.offsetHeight + element.scrollTop >
        element.scrollHeight - 200
      ) {
        ref.current++;
        setState({ page: ref.current });
      }
    }

    element.addEventListener("scroll", handler, false);

    return () => element.removeEventListener("scroll", handler, false);
  }, [element]);

  return (
    <>{props.children(props.source.slice(0, state.page * itemsPerPage))}</>
  );
}

export default observer(Virtualized);
