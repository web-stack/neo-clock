import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React from "react";
import IHasChildren from "~utils/IHasChildren";
import IHasClassName from "~utils/IHasClassName";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Card.scss";

interface IProps extends IHasChildren, IHasClassName {}

function Card(props: IProps) {
  const classes = useThemedClasses({
    light: styles.rootLight,
    dark: styles.rootDark,
  });
  // const animationClasses = useThemedClasses({
  //   light: styles.appear__light,
  //   dark: styles.appear__dark,
  // });

  return (
    <div className={clsx(styles.root, classes, props.className)}>
      {props.children}
    </div>
  );
}

export default observer(Card);
