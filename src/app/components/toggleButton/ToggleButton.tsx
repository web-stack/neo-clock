import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React, { useState } from "react";
import Cross from "~app/components/toggleButton/Cross";
import { useThemedClasses } from "~utils/Theming";
import styles from "./ToggleButton.scss";

interface IProps {
  initial?: boolean;
  onChange?: (isOn: boolean) => void;
}

function ToggleButton(props: IProps) {
  const [state, setState] = useState({ isOn: props.initial });
  const classes = useThemedClasses({
    light: state.isOn ? styles.disabledLight : styles.rootLight,
    dark: state.isOn ? styles.disabledDark : styles.rootDark,
  });
  const handleChange = () => {
    const isOn = !state.isOn;
    setState({ isOn });
    if (props.onChange) {
      props.onChange(isOn);
    }
  };

  return (
    <div className={clsx(styles.root, classes)} onClick={handleChange}>
      <Cross disabled={state.isOn} />
    </div>
  );
}

export default observer(ToggleButton);
