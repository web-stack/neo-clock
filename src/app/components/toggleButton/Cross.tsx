import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React from "react";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Cross.scss";

interface IProps {
  disabled?: boolean;
}

function Cross(props: IProps) {
  const classes = useThemedClasses({
    light: styles.rootLight,
    dark: styles.rootDark,
  });

  return (
    <div
      className={clsx(styles.root, classes, props.disabled && styles.disabled)}
    />
  );
}

export default observer(Cross);
