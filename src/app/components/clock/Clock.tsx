import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import Arrow from "~app/components/clock/Arrow";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Clock.scss";

interface IProps {
  date: Date;
}

function Clock(props: IProps) {
  const classes = useThemedClasses({
    light: styles.rootLight,
    dark: styles.rootDark,
  });
  const dotClasses = useThemedClasses({
    light: styles.dotLight,
    dark: styles.dotDark,
  });

  const angles = getAngles(props.date);

  return (
    <div className={clsx(styles.root, classes)}>
      <Arrow angle={angles.hours - 90} variant="hours" />
      <Arrow angle={angles.minutes - 90} variant="minutes" />
      <Arrow angle={angles.seconds - 90} variant="seconds" />
      <div className={clsx(styles.dot, dotClasses)} />
    </div>
  );
}

export default observer(Clock);

function getAngles(date: Date) {
  return {
    hours: date.getHours() * 30,
    minutes: date.getMinutes() * 6,
    seconds: date.getSeconds() * 6,
  };
}
