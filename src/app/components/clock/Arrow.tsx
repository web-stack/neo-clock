import clsx from "clsx";
import React, { useRef } from "react";
import { useThemedClasses } from "~utils/Theming";
import styles from "./Arrow.scss";

interface IProps {
  angle: number;
  variant: "hours" | "minutes" | "seconds";
}

function Arrow(props: IProps) {
  const element = useRef<HTMLDivElement | null>(null);
  const classes = useThemedClasses({
    dark: styles[`${props.variant}Dark`],
    light: styles[`${props.variant}Light`],
  });

  const dx =
    Math.cos(((props.angle + 90) * Math.PI) / 180) *
    (props.variant === "seconds" ? 1 : 2);
  const dy =
    Math.sin(((props.angle + 90) * Math.PI) / 180) *
    (props.variant === "seconds" ? 1 : 2);

  return (
    <div
      ref={element}
      className={clsx(styles.root, styles[props.variant], classes)}
      style={{
        transform: `rotate(${
          props.angle
        }deg) translate(${-dx}px, calc(${-dy}px)`,
      }}
    />
  );
}

export default Arrow;
