import { action, computed, observable, observe } from "mobx";
import { ISettingsService } from "~service/settings/SettingsService";

export type Theme = "light" | "dark";

export interface IAppStore {
  theme: Theme;
  date: Date;
  dateWithSeconds: Date;
  citySearch: string;
  citiesList: string[];

  toggleTheme(): void;

  addCity(city: string): void;

  removeCity(city: string): void;

  hasCity(city: string): boolean;
}

class AppStore implements IAppStore {
  @observable
  private _theme: Theme = "light";

  @observable
  private _date: Date = new Date();

  @observable
  private _citySearch: string = "";

  @observable
  private readonly _citiesList: string[] = [];

  /**
   * seconds introduced for performance reasons since most components only should react to minutes change
   */
  @observable
  private _seconds: number = 0;

  constructor(settingsService: ISettingsService) {
    const { theme, citiesList } = settingsService.loadSettings();
    this._theme = theme;
    this._citiesList = citiesList;
    this._seconds = this._date.getSeconds();

    setInterval(() => {
      this._seconds += 1;
      if (this._seconds === 60) {
        this._date = new Date();
        this._seconds = 0;
      }
    }, 1000);

    observe(this._citiesList, (change) => {
      settingsService.saveCities(change.object);
    });

    observe(this, "theme", (change) => {
      settingsService.saveTheme(change.newValue);
    });

    this.toggleTheme = this.toggleTheme.bind(this);
  }

  @computed
  get citiesList(): string[] {
    return this._citiesList;
  }

  set citiesList(value: string[]) {
    this._citiesList.splice(0, this._citiesList.length);
    this._citiesList.push(...value);
  }

  @computed
  get theme() {
    return this._theme;
  }

  @computed
  get date() {
    return this._date;
  }

  @computed
  get dateWithSeconds() {
    const date = new Date(this._date.getTime());
    date.setSeconds(this._seconds);
    return date;
  }

  set theme(value) {
    this._theme = value;
  }

  set citySearch(city) {
    this._citySearch = city;
  }

  @action
  toggleTheme() {
    this.theme = this.theme === "dark" ? "light" : "dark";
  }

  @computed
  get citySearch() {
    return this._citySearch;
  }

  @action
  addCity(city: string): void {
    this._citiesList.push(city);
  }

  @action
  removeCity(city: string): void {
    const i = this._citiesList.indexOf(city);
    if (typeof i === "undefined") {
      return;
    }

    this._citiesList.splice(i, 1);
  }

  public hasCity(city: string): boolean {
    return this.citiesList.indexOf(city) !== -1;
  }
}

export default AppStore;
