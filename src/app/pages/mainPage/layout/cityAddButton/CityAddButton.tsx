import React from "react";
import Button from "~app/components/button/Button";
import { useNavigationService } from "~service/navigation/NavigationServiceProvider";
import styles from "./CityAddButton.scss";
import { useHistory } from "react-router-dom";

interface IProps {}

function CityAddButton(props: IProps) {
  const history = useHistory();
  const navigationService = useNavigationService();
  const handleClick = () => {
    history.push("/cities-list");
    navigationService.forward();
  };

  return (
    <div className={styles.container}>
      <Button onClick={handleClick}>Add city</Button>
    </div>
  );
}

export default CityAddButton;
