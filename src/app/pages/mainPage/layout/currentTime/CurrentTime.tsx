import { observer } from "mobx-react-lite";
import React from "react";
import { useApplicationContext } from "~app/App";
import { dateToTimeString } from "~utils/date";
import styles from "./CurrentTime.scss";

function CurrentTime() {
  const store = useApplicationContext();
  const time = dateToTimeString(store.date);

  return (
    <div className={styles.container}>
      <h1 className={styles.root}>{time}</h1>
    </div>
  );
}

export default observer(CurrentTime);
