import { observer } from "mobx-react-lite";
import React from "react";
import { useApplicationContext } from "~app/App";
import { useTimezoneService } from "~service/timezone/TimezoneServiceProvider";
import styles from "./CitiesList.scss";
import CityCard from "~app/components/cityCard/CityCard";
import { addTimezone } from "~utils/date";

function CitiesList() {
  const store = useApplicationContext();
  const timezoneService = useTimezoneService();

  return (
    <div className={styles.container}>
      {timezoneService.timezonesByCityName(store.citiesList).map((timezone) => (
        <div key={timezone.zoneName} className={styles.cityCardContainer}>
          <CityCard
            city={timezone.cityName}
            time={addTimezone(store.date, -timezone.gmtOffset / 3600)}
          />
        </div>
      ))}
    </div>
  );
}

export default observer(CitiesList);
