import { observer } from "mobx-react-lite";
import React from "react";
import { useApplicationContext } from "~app/App";
import styles from "./AnalogClock.scss";
import Clock from "~app/components/clock/Clock";

function AnalogClock() {
  const store = useApplicationContext();
  return (
    <div className={styles.container}>
      <Clock date={store.dateWithSeconds} />
    </div>
  );
}

export default observer(AnalogClock);
