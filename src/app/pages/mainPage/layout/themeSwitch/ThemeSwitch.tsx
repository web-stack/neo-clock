import React from "react";
import { useApplicationContext } from "~app/App";
import Switch from "~app/components/switch/Switch";
import styles from "./ThemeSwitch.scss";

function ThemeSwitch() {
  const store = useApplicationContext();

  return (
    <div className={styles.container}>
      <h2>Dark mode</h2>
      <Switch
        isOn={store.theme === "dark"}
        onChange={() => store.toggleTheme()}
      />
    </div>
  );
}

export default ThemeSwitch;
