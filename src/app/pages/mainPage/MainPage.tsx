import clsx from "clsx";
import { observer } from "mobx-react-lite";
import React, { useEffect } from "react";
import AnalogClock from "~app/pages/mainPage/layout/analogClock/AnalogClock";
import CitiesList from "~app/pages/mainPage/layout/citiesList/CitiesList";
import CityAddButton from "~app/pages/mainPage/layout/cityAddButton/CityAddButton";
import CurrentTime from "~app/pages/mainPage/layout/currentTime/CurrentTime";
import ThemeSwitch from "~app/pages/mainPage/layout/themeSwitch/ThemeSwitch";
import { useTimezoneService } from "~service/timezone/TimezoneServiceProvider";
import { useThemedClasses } from "~utils/Theming";
import styles from "./MainPage.scss";

function MainPage() {
  const timezoneService = useTimezoneService();
  const classes = useThemedClasses({
    dark: styles.rootDark,
    light: styles.rootLight,
  });

  useEffect(() => {
    timezoneService.loadTimezones();
  }, []);

  return (
    <div className={clsx(styles.root, classes)}>
      <ThemeSwitch />
      <AnalogClock />
      <CurrentTime />
      <CitiesList />
      <CityAddButton />
    </div>
  );
}

export default observer(MainPage);
