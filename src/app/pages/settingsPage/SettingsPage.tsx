import React from "react";
import Button from "~app/components/button/Button";
import { useNavigationService } from "~service/navigation/NavigationServiceProvider";

interface IProps {}

function SettingsPage(props: IProps) {
  const navigationService = useNavigationService();
  return (
    <div>
      <h2>Settings</h2>
      <Button onClick={() => navigationService.backward()}>Back</Button>
    </div>
  );
}

export default SettingsPage;
