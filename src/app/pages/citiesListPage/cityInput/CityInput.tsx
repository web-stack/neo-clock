import { observer } from "mobx-react-lite";
import React from "react";
import { useApplicationContext } from "~app/App";
import Input from "~app/components/input/Input";
import styles from "./CityInput.scss";
import svg from "./img/searchIcon.svg";

interface IProps {}

function CityInput(props: IProps) {
  const appStore = useApplicationContext();
  const handleChange = (value: string) => {
    appStore.citySearch = value;
  };

  return (
    <div className={styles.container}>
      <Input
        placeholder="Search your city"
        value={appStore.citySearch}
        onChange={handleChange}
      />
      <img className={styles.searchIcon} src={svg} alt="search" />
    </div>
  );
}

export default observer(CityInput);
