import { observer } from "mobx-react-lite";
import React from "react";
import { useApplicationContext } from "~app/App";
import CityCard, { ICityCardProps } from "~app/components/cityCard/CityCard";
import ToggleButton from "~app/components/toggleButton/ToggleButton";
import styles from "./CityEntry.scss";

interface IProps extends ICityCardProps {
  cityIsSelected?: boolean;
}

function CityEntry(props: IProps) {
  const appStore = useApplicationContext();
  const handleToggleButton = (isOn: boolean) => {
    if (isOn) {
      appStore.addCity(props.city);
    } else {
      appStore.removeCity(props.city);
    }
  };

  return (
    <div className={styles.container}>
      <CityCard {...props} />
      <div className={styles.button__container}>
        <ToggleButton
          initial={props.cityIsSelected}
          onChange={handleToggleButton}
        />
      </div>
    </div>
  );
}

export default observer(CityEntry);
