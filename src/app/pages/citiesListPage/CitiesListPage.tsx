import { observer } from "mobx-react-lite";
import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useApplicationContext } from "~app/App";
import Virtualized from "~app/components/virtualized/Virtualized";
import CityEntry from "~app/pages/citiesListPage/cityEntry/CityEntry";
import CityInput from "~app/pages/citiesListPage/cityInput/CityInput";
import { useNavigationService } from "~service/navigation/NavigationServiceProvider";
import { useTimezoneService } from "~service/timezone/TimezoneServiceProvider";
import { addTimezone } from "~utils/date";

interface IProps {}

function CitiesListPage(props: IProps) {
  const history = useHistory();
  const appStore = useApplicationContext();
  const navigationService = useNavigationService();
  const timezoneService = useTimezoneService();
  const source = timezoneService.timezonesByCityName(appStore.citySearch);

  useEffect(() => {
    const disposer = history.listen(({ pathname }) => {
      if (pathname === "/") {
        navigationService.backward();
      }
    });

    return () => disposer();
  }, []);

  return (
    <>
      <CityInput />
      <Virtualized source={source}>
        {(items) =>
          items.map((timezone) => (
            <CityEntry
              key={timezone.zoneName}
              city={timezone.cityName}
              cityIsSelected={appStore.hasCity(timezone.cityName)}
              time={addTimezone(appStore.date, timezone.gmtOffset / 3600)}
            />
          ))
        }
      </Virtualized>
    </>
  );
}

export default observer(CitiesListPage);
