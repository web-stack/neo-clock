import { observer, useLocalStore } from "mobx-react-lite";
import React, { Context, useContext, useEffect } from "react";
import AppStore, { IAppStore } from "~app/App.store";
import ViewTransition from "~app/components/viewTransition/ViewTransition";
import { useNavigationService } from "~service/navigation/NavigationServiceProvider";
import { useSettingsService } from "~service/settings/SettingsServiceProvider";
import { useTimezoneService } from "~service/timezone/TimezoneServiceProvider";
import "~utils/Normalize.css";

const AppContext = React.createContext<IAppStore | null>(null);

function App() {
  const timezoneService = useTimezoneService();
  const settingsService = useSettingsService();
  const navigationService = useNavigationService();
  const store = useLocalStore(() => new AppStore(settingsService));

  useEffect(() => {
    timezoneService.loadTimezones();
  }, []);

  return (
    <AppContext.Provider value={store}>
      {navigationService.map((state, page) => {
        return (
          <ViewTransition key={page.name} viewState={state}>
            <page.Component />
          </ViewTransition>
        );
      })}
    </AppContext.Provider>
  );
}

export default observer(App);

export function useApplicationContext() {
  return useContext<IAppStore>(AppContext as Context<IAppStore>);
}
