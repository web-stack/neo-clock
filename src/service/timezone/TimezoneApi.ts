import api from "~config/api";
import GetAllResponse from "~service/timezone/response/GetAllResponse";

export interface ITimezoneApi {
  getAllTimeZones(): Promise<GetAllResponse>;
}

class TimezoneApi implements ITimezoneApi {
  async getAllTimeZones(): Promise<GetAllResponse> {
    const url = `${api.url}/list-time-zone?key=${api.apiKey}&format=json`;

    return await fetch(url).then((res) => res.json());
  }
}

export default TimezoneApi;
