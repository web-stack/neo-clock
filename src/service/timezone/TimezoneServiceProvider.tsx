import { useLocalStore } from "mobx-react-lite";
import React, { useContext } from "react";
import FileTimezoneApi from "~service/timezone/FileTimezoneApi";
import TimezoneServiceImpl, {
  TimezoneService,
} from "~service/timezone/TimezoneService";
import IHasChildren from "~utils/IHasChildren";

const timezoneApi = new FileTimezoneApi();
const timezoneService = new TimezoneServiceImpl(timezoneApi);

const Context = React.createContext<TimezoneService>(timezoneService);
Context.displayName = "TimezoneServiceContext";

interface IProps extends IHasChildren {}

function TimezoneServiceProvider(props: IProps) {
  const store = useLocalStore(() => timezoneService);

  return <Context.Provider value={store}>{props.children}</Context.Provider>;
}

export default TimezoneServiceProvider;

export function useTimezoneService() {
  return useContext(Context);
}
