import { action, computed, observable } from "mobx";
import Timezone from "~model/Timezone";
import WithCityName from "~model/WithCityName";
import { ITimezoneApi } from "~service/timezone/TimezoneApi";

export interface TimezoneService {
  readonly timezones: (Timezone & WithCityName)[];

  timezonesByCityName(cityName: string | string[]): (Timezone & WithCityName)[];
  loadTimezones(): Promise<Timezone[]>;
}

class TimezoneServiceImpl implements TimezoneService {
  private timezoneApi: ITimezoneApi;

  @observable
  private _timezones: (Timezone & WithCityName)[] | null = null;

  constructor(timezoneApi: ITimezoneApi) {
    this.timezoneApi = timezoneApi;
  }

  async loadTimezones(): Promise<Timezone[]> {
    if (this._timezones === null) {
      const res = await this.timezoneApi.getAllTimeZones();
      this.saveTimezones(res.zones);
    }

    return this.timezones;
  }

  timezonesByCityName(
    cityName: string | string[]
  ): (Timezone & WithCityName)[] {
    if (typeof cityName === "object") {
      return this.timezonesByCitiesList(cityName);
    }

    if (!cityName) return this.timezones;

    return this.timezones.filter((t) =>
      t.cityName.toLowerCase().includes(cityName.toLowerCase())
    );
  }

  private timezonesByCitiesList(citiesList: string[]) {
    if (citiesList.length === 0) return [];

    return this.timezones.filter((t) =>
      citiesList.some((city) => t.cityName.toLowerCase() === city.toLowerCase())
    );
  }

  @action
  private saveTimezones(timezones: Timezone[]) {
    this._timezones = timezones.map((zone) => {
      return {
        ...zone,
        cityName: zone.zoneName.split("/").pop()?.replace(/_/g, " ") || "",
      };
    });
  }

  @computed
  get timezones() {
    return (this._timezones || [])
      .slice()
      .sort((a, b) => a.cityName.localeCompare(b.cityName));
  }
}

export default TimezoneServiceImpl;
