import Timezone from "~model/Timezone";

export default interface GetAllResponse {
  status: string;
  message: string;
  zones: Timezone[];
}
