import GetAllResponse from "~service/timezone/response/GetAllResponse";
import { ITimezoneApi } from "~service/timezone/TimezoneApi";
import data from "./response/getAllResponse.json";

class FileTimezoneApi implements ITimezoneApi {
  async getAllTimeZones(): Promise<GetAllResponse> {
    return data;
  }
}

export default FileTimezoneApi;
