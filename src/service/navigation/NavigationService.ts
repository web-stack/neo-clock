import { action, computed, observable } from "mobx";
import { ReactNode } from "react";
import Page, { ViewState } from "~service/navigation/Page";

class NavigationServiceImpl {
  @observable
  public readonly pages: Page[];

  @observable
  private current = 0;

  constructor(pages: Page[] = []) {
    this.pages = pages;
  }

  public isCurrent(page: Page | string) {
    if (typeof page === "string") {
      return this.pages.findIndex((p) => p.name === page) === this.current;
    }
    return this.pages.indexOf(page) === this.current;
  }

  private isNext(page: Page) {
    return this.pages.indexOf(page) > this.current;
  }

  private isPrevious(page: Page) {
    return this.pages.indexOf(page) < this.current;
  }

  private getViewState(page: Page) {
    let viewState: ViewState | undefined = undefined;
    if (this.isPrevious(page)) {
      return "leaving";
    }

    if (this.isNext(page)) {
      return "hidden";
    }

    return viewState;
  }

  @action
  public forward() {
    this.current = Math.min(this.current + 1, this.pages.length - 1);
  }

  @action
  public backward() {
    this.current = Math.max(0, this.current - 1);
  }

  @action
  public setPage(page: Page) {
    const index = this.pages.findIndex((p) => p === page);
    if (index) {
      this.current = index;
    }
  }

  public map(
    consumer: (viewState: ViewState | undefined, page: Page) => ReactNode
  ): ReactNode[] {
    return this.pages.map((page) => {
      return consumer(this.getViewState(page), page);
    });
  }
}

export default NavigationServiceImpl;
