import { useLocalStore } from "mobx-react-lite";
import React, { useContext, useEffect } from "react";
import NavigationServiceImpl from "~service/navigation/NavigationService";
import pages from "~service/navigation/pages";
import IHasChildren from "~utils/IHasChildren";
import { useHistory } from "react-router-dom";

const navigationService = new NavigationServiceImpl(pages);
const Context = React.createContext(navigationService);
Context.displayName = "NavigationServiceContext";

function NavigationServiceProvider({ children }: IHasChildren) {
  const history = useHistory();
  const store = useLocalStore(() => navigationService);

  useEffect(() => {
    const page = store.pages.find(
      (page) => page.name === history.location.pathname
    );
    if (page) {
      store.setPage(page);
    }
  }, []);

  return <Context.Provider value={store}>{children}</Context.Provider>;
}

export default NavigationServiceProvider;

export function useNavigationService() {
  return useContext(Context);
}
