import React from "react";

export type ViewState =
  | "leaving" // if page is current and user goes forward, the current page leaves the view
  | "hidden"; // if page is NOT the root and must have offset

interface Page {
  name: string;
  state?: ViewState;
  Component: React.ComponentType<any>;
}

export default Page;
