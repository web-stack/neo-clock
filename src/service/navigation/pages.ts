import CitiesListPage from "~app/pages/citiesListPage/CitiesListPage";
import MainPage from "~app/pages/mainPage/MainPage";
import Page from "~service/navigation/Page";

const pages: Page[] = [
  {
    name: "/",
    Component: MainPage,
  },
  {
    name: "/cities-list",
    Component: CitiesListPage,
  },
];

export default pages;
