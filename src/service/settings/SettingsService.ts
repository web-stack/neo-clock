import { Theme } from "~app/App.store";
import Settings from "~model/Settings";

export interface ISettingsService {
  loadSettings(): Settings;

  saveCities(cities: string[]): void;
  saveTheme(theme: Theme): void;
}

class SettingsService implements ISettingsService {
  loadSettings(): Settings {
    const citiesJson = localStorage.getItem("cities");
    const citiesList = citiesJson ? JSON.parse(citiesJson) : [];
    const theme = (localStorage.getItem("theme") as Theme) || "light";

    return {
      citiesList,
      theme,
    };
  }

  saveCities(cities: string[]): void {
    localStorage.setItem("cities", JSON.stringify(cities));
  }

  saveTheme(theme: Theme) {
    localStorage.setItem("theme", theme);
  }
}

export default SettingsService;
