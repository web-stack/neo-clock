import React, { useContext } from "react";
import IHasChildren from "~utils/IHasChildren";
import SettingsService, { ISettingsService } from "./SettingsService";

const settingsService = new SettingsService();
const Context = React.createContext<ISettingsService>(settingsService);

interface IProps extends IHasChildren {}

function SettingsServiceProvider(props: IProps) {
  return (
    <Context.Provider value={settingsService}>
      {props.children}
    </Context.Provider>
  );
}

export default SettingsServiceProvider;

export function useSettingsService() {
  return useContext(Context);
}
